1. Types of tags?
Ans: 1. Pared tags- An HTML tag is known as a paired tag when the tag consists of an opening tag and a closing tag as its companion tag. 

2. Unpared tags- An HTML tag is called an unpaired tag when the tag only has an opening tag and does not have a closing tag or a companion tag.

3. Self-closing tags- Self-Closing Tags are those HTML tags that do not have a partner tag, where the first tag is the only necessary tag that 
is valid for the formatting.

4. Formatting tags: The HTML tags that help us in the formatting of the texts like the size of the text, font styles, making a text bold, etc. 
This is done using tags like <font>, <b>, <u>, etc. Tables, divisions, and span tags are also those tags that help format a web page or 
document and set the layout of the page.

5. Structure Tags
The HTML tags that help in structuring the HTML document are called Structure Tags. Description, head, html, title, body, etc., form the 
group of the page structure tags. The structure tags only assist in creating or forming the basic html page from the root; that is, they 
do not affect or has any hand in the formatting of texts.

6.Control tags- Control Tags
Another category of tags that can be created is ‘Control Tags’. The Script tags, radio buttons or checkboxes, the Form tags, etc., 
forms the control tags. These are the tags that are used in managing content or managing scripts or libraries that are external. All the form 
tags, drop-down lists, input text boxes, etc., are used in interacting with the visitor or the user.

2. Head and body tags?
Ans:The <head> element is a container for metadata (data about data) and is placed between the <html> tag and the <body> tag.
Metadata is data about the HTML document. Metadata is not displayed. Metadata typically define the document title, character set, styles,
scripts, and other meta information.

3. Input controls and attributes?
Ans: Input (Button) control: INPUT type="button" element

Input (Checkbox) control: INPUT type="checkbox" element

Input (File) control: INPUT type="file" element

Input (Hidden) control: INPUT type="hidden" element

Input (Password) control: INPUT type="password" element

Input (Radio) control: INPUT type="radio" element

Input (Reset) control: INPUT type="reset" element

Input (Submit) control: INPUT type="submit" element

Input (Text) control: INPUT type="text" element

Attributes:   value- The input value attribute specifies an initial value for an input field
              disable- The input disabled attribute specifies that an input field should be disabled.A disabled input field is unusable
                       and un-clickable.
              read-only- The input readonly attribute specifies that an input field is read-only. A read-only input field cannot be modified
              (however, a user can tab to it, highlight it, and copy the text from it).

4. How to divide webpage with div tags?
Ans: How to use div tag in HTML to divide the page
Step 1: Add the div tags. Let's say you want to divide the page into three sections. ...
Step 2: Add the class name to div tags. The next step is to add the class name to the div tags and give them a unique name. ...
Step 3: Add the contents. At this point, you're all set!

5. What is Formatting tags?
Ans: Formatting tags: The HTML tags that help us in the formatting of the texts like the size of the text, font styles, making a text bold, etc. 
This is done using tags like <font>, <b>, <u>, etc. Tables, divisions, and span tags are also those tags that help format a web page or 
document and set the layout of the page.

6. How to create orderd and unorderd lists?
Ans: An ordered list is created using <ol> tags that stand for the overall ordered list, which then wrap around the rest of the
 individual <li> tags. Similarly, unordered lists use <ul> tags that stand for the overall unordered list, which then wrap around the 
individual <li> tags.

7. Table tags?
Ans: 

8. Form tag?
Ans: 

9. Frame tag?
Ans: The <frame> tag was used in HTML 4 to define one particular window (frame) within a <frameset>.
Use the <iframe> tag to embed another document within the current HTML document:
<iframe src="https://www.w3schools.com"></iframe>

10. How to provide meta keywords?
Ans: <meta name="viewport" content="width=device-width, initial-scale=1.0">

11. What is meta description?
Ans: The <meta> tag defines metadata about an HTML document. Metadata is data (information) about data.
<meta> tags always go inside the <head> element, and are typically used to specify character set, page description, keywords, 
author of the document, and viewport settings.