4. background-color --Opacity (0.0-1.0),  rgba(0, 128, 0, 0.3) can be assigened in % also
5. background-image --body {
  background-image: url("paper.gif");
}

6. background-repeat   --Eg 2 --body {
  background-image: url("img_tree.png");
  background-repeat: no-repeat;
  background-position: right top;
}

7. background-attachment, background-position-- 	Sets whether a background image is fixed or scrolls with the rest of the page
   body {
  background-image: url("img_tree.png");
  background-repeat: no-repeat;
  background-position: right top;
  background-attachment: scroll;
}

8. background shorthand --When using the shorthand property the order of the property values is:

background-color
background-image
background-repeat
background-attachment
background-position

1. font-family --In CSS there are five generic font families:

Serif fonts have a small stroke at the edges of each letter. They create a sense of formality and elegance.
Sans-serif fonts have clean lines (no small strokes attached). They create a modern and minimalistic look.
Monospace fonts - here all the letters have the same fixed width. They create a mechanical look. 
Cursive fonts imitate human handwriting.
Fantasy fonts are decorative/playful fonts.
All the different font names belong to one of the generic font families. 

2. color -- property change the color of fonts.
3. font-size
9. font-style
10. font-variant
11. font-weight
